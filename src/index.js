'use strict';

var Alexa = require("alexa-sdk");
var config = require('./config');
var intentHandlers = require('./intentHandlers');
var eventHandlers = require('./eventHandlers');
var stateHandlers = require('./stateHandlers');
var speechHandlers = require('./speechHandlers');

exports.handler = function(event, context, callback) {

  var alexa = Alexa.handler(event, context);
  alexa.appId = config.appId;

  alexa.registerHandlers(
    eventHandlers,
    stateHandlers.startModeIntentHandlers,
    //stateHandlers.resultsModeIntentHandlers,
    intentHandlers,
    speechHandlers
  );

  alexa.execute();

};
