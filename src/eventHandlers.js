'use strict';

var constants = require('./constants');
var config = require('./config');

var eventHandlers = {

  'NewSession': function(){

    this.handler.state = constants.states.START_MODE;
    
    if(this.event.request.type === 'LaunchRequest') {
      this.emit('welcomeStartSearch');
    } else if(this.event.request.type === 'IntentRequest') {
      var intentName = this.event.request.intent.name;
      this.emitWithState(intentName);
    } else {
      console.log('Unexpected request : ' + this.event.request.type);
    }

  },

  'EndSession': function(message){
    message = message || constants.terminate;
    this.emit(':tell', message);
  }

}

module.exports = eventHandlers;
