'use strict';

var Alexa = require('alexa-sdk');
var constants = require('./constants');
var config = require('./config');

var stateHandlers = {

    startModeIntentHandlers: Alexa.CreateStateHandler(constants.states.START_MODE, {
        /*
         *  All Intent Handlers for state : _START_MODE
         */

        'SearchRestaurantsIntent': function(){
          this.emit('startSearch');
        },

        /*'AMAZON.HelpIntent': function(){},*/
        'AMAZON.StopIntent': function(){
            this.emit('EndSession', constants.terminate);
        },

        'AMAZON.CancelIntent': function(){
            this.emit('EndSession', constants.terminate);
        },

        'SessionEndedRequest': function(){
            this.emit('EndSession', constants.terminate);
        },

        'UnhandledIntent': function(){
          this.emit('unhandled');
        }


    }),

    searchModeIntentHandlers: Alexa.CreateStateHandler(constants.states.SEARCH_MODE, {}),

    resultsModeIntentHandlers: Alexa.CreateStateHandler(constants.states.RESULTS_MODE, {
        /*
         *  All Intent Handlers for state : _RESULTS_MODE
         */
        //'AMAZON.YesIntent': function(){},
        //'AMAZON.NoIntent': function(){},
        //'AMAZON.NextIntent': function(){},
        //'AMAZON.PreviousIntent': function(){},
        //'AMAZON.StartOverIntent': function(){},

    })

};

module.exports = stateHandlers;
