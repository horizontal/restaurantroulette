"use strict";

module.exports = Object.freeze({
  //  States
  states : {
    START_MODE: '_START_MODE',
    SEARCH_MODE: '_SEARCH_MODE',
    RESULTS_MODE: '_RESULTS_MODE'
  },

  terminate: 'OK.',

  //  Speech break time
  breakTime : {
    '50' : '<break time = "50ms"/>',
    '100' : '<break time = "100ms"/>',
    '200' : '<break time = "200ms"/>',
    '250' : '<break time = "250ms"/>',
    '300' : '<break time = "300ms"/>',
    '500' : '<break time = "500ms"/>'
  }

});
