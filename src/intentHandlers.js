'use strict';

var request = require('request');
var rp = require('request-promise');
var https = require('https');
var twilio = require('twilio');
var googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyBfLKCsgTy2bowmmUfgqey_XrtxD6YfHPg',
  Promise: Promise
});

var constants = require('./constants');
var config = require('./config');


var intentHandlers = {

  'welcomeStartSearch': function(){
    var speechOutput = 'Welcome to Restaurant Roulette. What are you in the mood to eat? ';
    var timeofday; //parse time for 'this morning' < 11, tonight > 5, or today;
    var repromptSpeech = 'You can say things like Hamburgers or Chinese Food.';
    this.emit(':ask', speechOutput, repromptSpeech);
  },

  'startSearch': function(){

    var _self = this;
    var req = this.event.request;
    //console.log(this.event.context.System.user);
    //console.log(this.event);

    var intentObj = this.event.request.intent;

    var cuisineValue;
    var locationValue;

    var speechOutput;
    var repromptSpeech;
    var updatedIntent = 'SearchRestaurantsIntent';

    if(intentObj.slots.cuisine && intentObj.slots.cuisine.value){
      cuisineValue = intentObj.slots.cuisine.value;
    } else {
      speechOutput = 'What are you in the mood to eat?';
      repromptSpeech = 'You can say things like Hamburgers or Chinese Food.';
      this.emit(':elicitSlot', 'cuisine', speechOutput + repromptSpeech, repromptSpeech, updatedIntent);
    }


    var deviceId = this.event.context.System.device.deviceId;
    var consentToken = this.event.context.System.user.permissions.consentToken;

    var path = '/v1/devices/'+ deviceId +'/settings/address';

    var options = {
      uri: 'https://api.amazonalexa.com' + path,
      'headers': {
        'Authorization': 'Bearer ' + consentToken
      }
    }

    rp(options)
    .then(function(res){
      var obj = JSON.parse(res);
      geoCode(obj.postalCode,cuisineValue,_self);
    })
    .catch((err) => console.error(err));
    //getResults(true,options,handleDeviceIdResponse,_self);

  },

  'sendText': function() {

    // this should get populated with real data
    var smsMessageInfo = {
        "smsFromPhoneNumber" : "+19526496799",
        "smsToPhoneNumber" : "+16124195819",
        "smsFoodType" : "pizza",
        "smsBodyMessage" : "Winner winner %%foodtype%% dinner!",
        "smsLink" : "https://www.google.com/maps/place/Pizza+Luc%C3%A9/@44.9073984,-93.309592,12z/data=!4m8!1m2!2m1!1spizza+luce!3m4!1s0x87f6267a3d1e6a1d:0x21dee70f5bde7839!8m2!3d44.8834876!4d-93.2899017"
    };

    sendText(smsMessageInfo);

  }

}

module.exports = intentHandlers;


//Geocode Address data
function geoCode(zip,food,scope) {

  googleMapsClient.geocode({
    address: zip
  }).asPromise()
  .then(function(res){
    var location = res.json.results[0].geometry.location;
    getPlaces(location,food,scope);
  })
  .catch((err) => {
    console.log(err);
  });
}

//Search Places API
function getPlaces(location,query,scope) {

  console.log(location,query);

  googleMapsClient.places({
    query: query,
    language: 'en',
    location: [location.lat,location.lng],
    radius: 24,
    minprice: 2,
    maxprice: 4,
    opennow: true,
    type: 'restaurant'
  }, function(err,response) {
    if (!err) {
      console.log(response.json.results);
      var speechOutput = 'Shoot dog, we finally got all the data, but ran out of time.'
      scope.emit(':tell',speechOutput);
    } else {
      console.log(err);
    }
  });

}

// this is the logic to format and send the text message via twilio
function sendText(smsMessageInfo) {
  var accountSid = 'ACd16c144cb9993cf9a5882b56ddfc02a7'; // Your Account SID from www.twilio.com/console
  var authToken = '8a6a31af5899d3e928adbec02d05a6d0';   // Your Auth Token from www.twilio.com/console

  var client = new twilio(accountSid, authToken);

  client.messages.create({
      body: smsMessageInfo.smsBodyMessage.replace("%%foodtype%%", smsMessageInfo.smsFoodType) + " " + smsMessageInfo.smsLink,
      to: smsMessageInfo.smsToPhoneNumber,  // Text this number
      from: smsMessageInfo.smsFromPhoneNumber // From a valid Twilio number
  }).then((message) => console.log(message.sid));
}
