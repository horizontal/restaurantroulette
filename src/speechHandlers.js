'use strict';

var constants = require('./constants');
var config = require('./config');

var speechHandlers = {

  'welcome': function(){
    var speechOutput = 'Having trouble deciding where to eat, welcome to Restaurant Roulette. What are you in the mood to eat? ';
    var timeofday; //parse time for 'this morning' < 11, tonight > 5, or today;
    var repromptSpeech = 'You can say things like Hamburgers or Chinese Food.';
    var updatedIntent = 'SearchRestaurantsIntent';
    //this.emit(':elicitSlot', 'cuisine', speechOutput + repromptSpeech, repromptSpeech, updatedIntent);
    this.emit(':ask', speechOutput, repromptSpeech);
  },

  'unhandled': function(){
    var speechOutput = 'I\'m sorry, I didn\'t quite get that.';
    this.emit(':ask', speechOutput, speechOutput);
  }

}

module.exports = speechHandlers;
